import random

from discord import Game

from mechascrypt import parse, exceptions, PARSABLE_INSTRUCTIONS
from mechascrypt.library import *
from mechascrypt.wrap import wrap

from toby.config import config
from toby.client import client
from toby.docs import __version__, __doc__

from toby.commands.adjust import adjust
from toby.commands.summon import summon

from toby.utils.attr import rgetattr

@client.event
async def on_ready():
	await client.change_presence(activity=Game("what do you mean? i'm always online"))

	config.home_guild = client.get_guild(config.home_guild_id)
	config.welcome_channel = client.get_channel(config.welcome_channel_id)
	config.test_channel = client.get_channel(config.test_channel_id)

	print(config.home_guild)

	print(f'{client.user.name} online.')

	if config.tool:
		await __import__(f"toby.tools.{config.tool}", fromlist=["main"]).main()
		exit('Successfully completed task.')

@client.event
async def on_message(message):
	if message.author == client.user:
		return
	
	spontaneous = random.random() < config.generation.spontaneity or rgetattr(message, "reference.resolved.author", None) == client.user

	result = None # TODO: i hate this, we gotta find a better way

	## TOBY STANDARD LIBRARY
	commands = [adjust, summon]

	for command in wrap(commands, message):
		globals()[command.__name__] = command
    
	if any(message.content.startswith(instruction) for instruction in PARSABLE_INSTRUCTIONS):
		try:
			program = parse(message.content, execute=False)

			result = await eval(compile(program, filename="<ast>", mode="eval"))
		except exceptions.UnexpectedToken as e:
			print(e)
			# result = style('WHAT prithee cast thou!!')
			return
		except exceptions.CommandNotFoundError as e:
			print(e)
			return
		except exceptions.DocsNotFoundError as e:
			print(e)
			result = "nah. i know what that is. no docs though. don't got em"
		except NameError as e:
			print(e)
			# result = style('i have NOT been made familiar with such a spelle !')
			return
		except PermissionError as e:
			print(e)
			result = "dogg you CANNOT be out here doin that"
		except Exception as e:
			print(e)
			result = f"you dumbass:\n\n {e}"
	elif rgetattr(message, "reference.resolved.author", None) == client.user:
		result = await summon(demon="toby", to="SPONTANEOUS", message=message)
	elif spontaneous:
		result = await summon(message=message, to="SPONTANEOUS")

	if result:
		await message.channel.send(result)