import subprocess

__version__ = '3.0.0'
__branch__ = 'main'
__hash__ = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode('ascii').strip() # depends on git

__doc__ = f"""
T.O.B.U.S.Cu.S version v{__version__} (`{__hash__[:8]}`), activated
"""