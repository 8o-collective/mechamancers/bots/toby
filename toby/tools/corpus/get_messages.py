import aiohttp

import discord

from toby import config

async def total_messages():
	if not config.auth.user_discord_token:
		print("Not getting total amount of messages... if you would like to get the approximate total number of messages, add a user token to user_discord_token in auth.json. USE AN ALT, I HAVE NO IDEA IF THIS WILL GET THAT ACCOUNT BANNED.")
		return

	headers = {
		'Authorization' : config.auth.user_discord_token,
		'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) discord/0.0.269 Chrome/91.0.4472.164 Electron/13.6.6 Safari/537.36'
	}
	
	async with aiohttp.ClientSession(headers=headers) as session:
		async with session.get(f"https://discord.com/api/guilds/{config.home_guild_id}/messages/search") as r:
			return (await r.json())["total_results"]

async def get_messages():
	corpus_channels = [channel for channel in config.home_guild.channels if channel.__class__ == discord.channel.TextChannel and channel.id not in config.ignore_channels]

	for channel in corpus_channels:
		async for message in channel.history(limit=None):
			yield message