import asyncio
import os
import json

import tqdm.asyncio

import discord

from toby import config
from toby import client

from toby.utils.process import process_message

from .corpus.get_messages import total_messages, get_messages

dataDirectory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../../data")
os.makedirs(dataDirectory, exist_ok=True)

async def generate_corpus():	
	requested_messages = [message async for message in tqdm.asyncio.tqdm(get_messages(), total=await total_messages())]

	process_message_tasks = [asyncio.create_task(process_message(message if not message.author.bot else None)) for message in requested_messages]
	processed_messages = await asyncio.gather(*process_message_tasks)

	corpus = sorted([message for message in processed_messages if message], key=lambda message: message["creation_time"])

	with open(os.path.join(dataDirectory, "corpus.txt"), 'w+') as f:
		f.write("\n".join([f"{message['tags']} {message['content']}" for message in corpus]))
	
	formatted_corpus = []

	for i, message in enumerate(corpus):
		context = "\\n".join([f"{past['tags']} {past['content']}" for past in corpus[i - config.generation.context:i]])
		prompt = f"{context}\\n{message['tags']}"
		completion = f" {message['content']}{config.generation.delimiter}"
		formatted_corpus += [f"""{{"prompt": "{prompt}", "completion": "{completion}"}}"""]
	
	with open(os.path.join(dataDirectory, "corpus.jsonl"), 'w+') as f:
		f.write("\n".join(formatted_corpus))

async def main():
	return await generate_corpus()