import argparse

import os
import re
import json
from typing import Optional

from pydantic import BaseModel, create_model
from discord import Guild, TextChannel, Role
from re import Pattern

parser = argparse.ArgumentParser(prog='toby')
parser.add_argument("--tool")
args = parser.parse_args()

botDirectory = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../')

class TobyConfig(BaseModel):
	auth: create_model('Auth',
		discord_token=(str, ...),
		user_discord_token=(str, ...),
		openai_token=(str, ...),
	)

	home_guild_id: Optional[int] 			= None
	home_guild: Optional[Guild] 			= None

	welcome_channel_id: Optional[int] 		= None
	welcome_channel: Optional[TextChannel] 	= None

	test_channel_id: Optional[int] 			= None
	test_channel: Optional[TextChannel] 	= None

	engine: Optional[str]					= None

	ignore_channels: Optional[list[int]]	= None

	alts: Optional[dict] 					= None

	tool: Optional[str]						= args.tool

	test: int = 12

	class Generation(BaseModel):
		context: int = 4
		delimiter: str = "\n" # https://help.openai.com/en/articles/5072263-how-do-i-use-stop-sequences
		min_tokens: int = 32
		max_tokens: int = 128
		temperature: float = 1

		spontaneity: float = 0.0005

		class Config:
			validate_assignment = True
	
	generation: Generation = Generation()

	class Tools():
		digits: int = 5

		class Regex():
			link: Pattern = re.compile(r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)") # https://stackoverflow.com/a/3809435
			user_ping: Pattern = re.compile(r"<@!?([0-9]+)>")
			role_ping: Pattern = re.compile(r"<@&?([0-9]+)>")
			emote: Pattern = re.compile(r"<(:\w+:)[0-9]+>")
			channel: Pattern = re.compile(r"<#([0-9]+)>")
		
		regex: Regex = Regex()

	tools: Tools = Tools()

	class Config:
		arbitrary_types_allowed = True

try:
	with open(os.path.join(botDirectory, "auth.json"), 'r') as f:
		auth = json.load(f)
except FileNotFoundError:
	raise PermissionError('You do not have an authentication file. Please load auth.json from the AUTH project variable.')

with open(os.path.join(botDirectory, "toby.config.json"), 'r') as f:
	config = TobyConfig(**{'auth': auth, **json.load(f)})