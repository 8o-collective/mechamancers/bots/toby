from toby import config

from toby.utils.attr import rsetattr, rgetattr

async def adjust(setting=None, value=None, message=None):
	'''
	Adjust divine parameters.

	Parameters
	----------
	setting : str
		name of setting to modify
	value : Union[str, int, float]
		the value to change the setting to. if not passed, returns the current value of the setting
	'''
	setting = setting.lower()

	if value is not None:
		rsetattr(config.generation, setting, value)
		return f"set {setting} to {value}"
	else:
		value = rgetattr(config.generation, setting, None)
		if value is not None:
			return f"setting {setting} currently set to {value}"
		else:
			return f"invalid setting {setting}"
