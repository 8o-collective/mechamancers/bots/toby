import asyncio
import aiohttp
from enum import Enum, auto
import random

from toby import config

from toby.utils.tags import Tags
from toby.utils.process import process_message

completion_endpoint = "https://api.openai.com/v1/completions"

headers = {
	"Content-Type": "application/json",
	"Authorization": f"Bearer {config.auth.openai_token}"
}
class Purpose(Enum):
	ASK = auto()
	RESPOND = auto()
	PROMPT = auto()

	SPONTANEOUS = auto()

async def summon(demon=None, to="RESPOND", line=None, message=None):
	'''
	Communicate with demons.

	Parameters
	----------
	demon : str
		name used in generation
	to : [ask, respond, prompt]
		the purpose of summoning the demon
	line : str
		the prompt to add in generation (only used for ASK and PROMPT)
	'''
	async with message.channel.typing():
		name = demon

		if name == None:
			name = message.author.name

		tags = Tags(message)
		tags.dict["By"] = name

		purpose = Purpose[to.upper()]

		if purpose is Purpose.SPONTANEOUS:
			history = [message async for message in message.channel.history(limit=config.generation.context)]

			if name is message.author.name:
				tags.dict["By"] = random.choice([member for member in config.home_guild.members if any(["tz|" in role.name for role in member.roles]) and member.name != message.author.name]).name

			tags.dict["Re"] = message.author.name
		elif purpose is Purpose.RESPOND:
			history = [message async for message in message.channel.history(limit=config.generation.context + 1)][1:]
		elif purpose is Purpose.ASK:
			message.content = f"@{name} {line}"

			if name is message.author.name:
				message.author = random.choice([member for member in config.home_guild.members if any(["tz|" in role.name for role in member.roles]) and member.name != message.author.name])
			
			tags.dict["Re"] = message.author.name

			history = [message]
		else:
			history = []
	
		tags = repr(tags)
		
		process_message_tasks = [asyncio.create_task(process_message(message)) for message in history]
		processed_messages = sorted([message for message in await asyncio.gather(*process_message_tasks) if message], key=lambda message: message["creation_time"])

		context = "\\n".join([f"{message['tags']} {message['content']}" for message in processed_messages])
	
		if purpose is not Purpose.PROMPT:
			prompt = context + "\\n" + tags
		else:
			prompt = f"{tags} {line}"
		
		data = {
			"model": config.engine,
			"prompt": prompt,

			"stop": [config.generation.delimiter, "\\n"],

			"max_tokens": config.generation.max_tokens,
			"temperature": config.generation.temperature
		}

		generations = []

		async with aiohttp.ClientSession(headers=headers) as session:
			while sum(len(generation) for generation in generations) < config.generation.min_tokens:
				async with session.post(completion_endpoint, json=data) as r:
					response = await r.json()
					generation = str(response["choices"][0]["text"])
					data["prompt"] += generation + "\\n" + tags
					generations += [generation]

		if purpose is Purpose.PROMPT:
			return tags + " " + line + f"\n{tags}".join(generations)

		return "\n".join([f"{tags}{generation}" for generation in generations])