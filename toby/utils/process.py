"""
This file generates a history of the server that includes which user is the author of each message.

In other words, the model trained off this data will produce messages that simulate each individual user rather than the aggregate of the entire server.
"""
import json
import re

import discord

from toby import config
from toby import client

from toby.utils.alts import check_alt
from toby.utils.tags import Tags

from .materials import get_url_title

FIRST_LETTER_NONALPHANUM=[
	'<', # Pings start with this
	# '`', # Code backticks
	'\'', # Quotes
	'"',
	' ', # Space
]

def sanitize_content(content):
	return json.dumps(content.replace("\n", " ").replace("\\", "")).strip('"').replace("/", "\/").replace("\\\"", "\\\\\\\"").replace(config.generation.delimiter, "DELIMITER")

async def process_content(content):
	"""
	Take a discord.Message object and return a string.
	"""
	original_length = len(content)
	offset = lambda: len(content) - original_length
	replace_match = lambda m, new: content[:m.start(0) + offset()] + new + content[m.end(0) + offset():]

	# intentionally not handling images, those are removed from the dataset

	for m in config.tools.regex.link.finditer(content):
		description = await get_url_title(m.group())
		message_description = f'<Link: {description}>' if description else ''
		content = replace_match(m, message_description)

	for m in config.tools.regex.user_ping.finditer(content):
		content = replace_match(m, f"@{getattr(check_alt(client.get_user(int(m.group(1)))), 'name', None)}")
	
	for m in config.tools.regex.role_ping.finditer(content):
		content = replace_match(m, f"@{getattr(config.home_guild.get_role(int(m.group(1))), 'name', None)}")
	
	for m in config.tools.regex.channel.finditer(content):
		content = replace_match(m, f"#{getattr(config.home_guild.get_channel_or_thread(int(m.group(1))), 'name', None)}")
	
	for m in config.tools.regex.emote.finditer(content):
		content = replace_match(m, m.group(1))
	
	content = sanitize_content(content) # sanitize

	if any(c.isalnum() for c in content): # check if any alphanumberic character
		return content
	else:
		return None

async def process_message(message):
	"""
	Take a discord.Message object and return a string.
	"""
	# if message.author.bot: # delete all bot messages
	# 	return None
	
	if message.content == '':
		return None

	# if not message.content[0].isalnum() and message.content[0] not in FIRST_LETTER_NONALPHANUM: # and all invocations
	# 	return ''

	# if message.reference:
	# 	print(message.reference.resolved.author.name)
	
	tags = Tags(message)

	content = await process_content(message.content)

	if content:
		return {
			"tags": repr(tags),
			"content": content,
			"creation_time": message.created_at
		}
	else:
		return None