from toby import client
from toby import config

# Restructure dict for faster lookup
alts = {int(alt): int(original) for original, alts in config.alts.items() for alt in alts}

def check_alt(user):
	return user if user is None or user.id not in alts.keys() else client.get_user(alts[user.id])