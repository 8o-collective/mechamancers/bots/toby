import re
import html
import aiohttp

import discord

from toby import config
from toby import client

EXCLUDE_HOSTS = [
	'discord',
	'tenor',
	'lichess',
	'tinder'
]

TITLE_REGEX = re.compile(r'<title>(.*?)</title>', re.UNICODE)

async def get_url_title(url):
	if any(host in url for host in EXCLUDE_HOSTS):
		return None # Image link

	try:
		async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(total=60)) as session:
			async with session.get(url) as r: # verify?
				if "text/html" not in r.headers.get("Content-Type", []):
					return None # Content link

				if r.ok:
					match = TITLE_REGEX.search(await r.text())
					if match:
						return html.unescape(match.group(1))
					return None
				return r.status
	except:
		return None