from toby import config

from .alts import check_alt
from .attr import rgetattr

# def get_author(message):
	# return getattr(message.reference.resolved, "author", None) if message.reference and message.reference.resolved else None

class Tags():
	def __init__(self, message):
		self.dict = {
			"By": check_alt(message.author).name,
			"In": getattr(message.channel, "name", config.welcome_channel.name) if message.channel.id not in config.ignore_channels else config.welcome_channel.name,
			"At": message.created_at.time().strftime("%H:%M"),
			"Re": rgetattr(message, "reference.resolved.author.name", None), # author name if reply else None
		}
	
	def __repr__(self) -> str:
		return ' '.join(f'{k}:{v}' for k, v in self.dict.items() if v is not None)